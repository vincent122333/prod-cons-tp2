package ca.qc.claurendeau.storage;

import ca.qc.claurendeau.exception.*;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BufferTest {
    
    private static final int INITIAL_BUFFER_CAPACITY = 15;
    private static final int DATA_TEST = 10;
    private static final int DATA_TEST2 = 44;
    private Buffer buffer;
    private Element element;
    private Element element2;
    
    @Before
    public void setUp() throws Exception {
        buffer = new Buffer(INITIAL_BUFFER_CAPACITY);
        element = new Element(DATA_TEST);
        element2 = new Element(DATA_TEST2);
    }

    @Test
    public void testBuildSuccess() {
        assertTrue(true);
    }
    
    @Test
    public void testCreationBufferWithGivenInitialCapacity() {
        assertTrue("La capacite du buffer n'a pas ete initialis� correctement avec la capacite de : "+INITIAL_BUFFER_CAPACITY+"", 
                    buffer.capacity() == INITIAL_BUFFER_CAPACITY);
    }
    
    @Test
    public void testAddElementToBuffer() throws BufferFullException {
        buffer.addElement(element);
        assertTrue("L'ajout D'un �l�ment au buffer n'a pas fonctionn�", buffer.getCurrentLoad() == 1);
    }
    
    @Test(expected = BufferEmptyException.class)
    public void testRevoveElementInEmptyBuffer() throws BufferEmptyException{
        buffer.removeElement();
        assertTrue("L'enl�vement d'un �l�ment dans une bufer vide n'est pas impl�ment� comme il faut", buffer.isEmpty());
    }
    
    @Test(expected = BufferFullException.class)
    public void testAddElementInFullBuffer() throws BufferFullException{
        for(int i = 0; i < INITIAL_BUFFER_CAPACITY + 1; i++) {
            buffer.addElement(new Element(i));
        }
        
        assertTrue("L'ajout d'un �l�ment dans un buffer plein n'est pas impl�ment� comme il faut", buffer.isFull());
    }
    
    @Test
    public void testToStringBuffer() throws BufferFullException {
        buffer.addElement(element);
        buffer.addElement(element2);
        assertEquals("La m�thode toString de la classe Buffer n'est pas ipl�ment� comme il faut", buffer.toString(),
                     "[" + DATA_TEST + ", " + DATA_TEST2 + "]");
    }

}


