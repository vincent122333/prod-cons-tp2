package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ElementTest {
    
    private static final int ELEMENT_DATA_TEST = 10;
    private Element element;
    
    @Before
    public void setUp() throws Exception {
        element = new Element(ELEMENT_DATA_TEST);
    }

    @Test
    public void testGetDataElement() {
        assertEquals("La fonction getData() n'a pas �t� impl�ment� de la bonne fa�on", element.getData(), ELEMENT_DATA_TEST);
    }
    
    @Test
    public void testSetdataElement() {
        element.setData(23);
        assertEquals("La fonction getData() n'a pas �t� impl�ment� de la bonne fa�on", element.getData(), 23);
    }
}
