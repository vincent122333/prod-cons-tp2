package ca.qc.claurendeau.exception;

public class BufferEmptyException extends Exception {
    // VOTRE CODE
    public BufferEmptyException() {
        super("Buffer is empty");
    }
}
