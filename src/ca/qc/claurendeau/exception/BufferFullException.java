package ca.qc.claurendeau.exception;

public class BufferFullException extends Exception {
    // VOTRE CODE
    public BufferFullException() {
        super("Buffer is full");
    }
}
