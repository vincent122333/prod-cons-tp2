package ca.qc.claurendeau.storage;

import java.util.ArrayList;
import java.util.List;

import ca.qc.claurendeau.exception.*;

public class Buffer
{
    private int capacity;
    private List<Element> liste_elements;

    public Buffer(int capacity)
    {
        this.capacity = capacity;
        this.liste_elements = new ArrayList<>();
    }

    // returns the capacity of the buffer
    public int capacity()
    {
        return capacity;
    }

    // returns the number of elements currently in the buffer
    public int getCurrentLoad()
    {
        return liste_elements.size();
    }

    // returns true if buffer is empty, false otherwise
    public boolean isEmpty()
    {
        return liste_elements.size() == 0;
    }

    // returns true if buffer is full, false otherwise
    public boolean isFull()
    {
        return liste_elements.size() == capacity;
    }

    // adds an element to the buffer
    // Throws an exception if the buffer is full
    public synchronized void addElement(Element element) throws BufferFullException
    {
        if(isFull()){
            throw new BufferFullException();
        }
        liste_elements.add(element);
    }
    
    // removes an element and returns it
    // Throws an exception if the buffer is empty
    public synchronized Element removeElement() throws BufferEmptyException
    {
        if (isEmpty()) {
             throw new BufferEmptyException();
        }
        return liste_elements.remove(0);
    }
    
    // returns the content of the buffer in form of a string
    public String toString()
    {
        return liste_elements.toString();
    }
    
}
