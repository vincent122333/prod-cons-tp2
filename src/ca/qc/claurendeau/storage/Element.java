package ca.qc.claurendeau.storage;

public class Element
{
    private int data;
    
    public Element() {
        super();
    }
    
    public Element(int data) {
        super();
        this.data = data;
    }

    public void setData(int data)
    {
        this.data = data;
    }
    
    public int getData()
    {
        return data;
    }
    
    @Override
    public String toString() {
        return getData() + "";
    }
}
